import threading
import requests 
import os
import base64
import traceback
import json

import cv2
import numpy as np
from flask import Flask, render_template, Response, request, make_response, current_app, jsonify
from werkzeug.utils import secure_filename
from botocore.config import Config
from decouple import config
from datetime import datetime, timedelta
from functools import update_wrapper

from darkflow.net.build import TFNet
from odhen.utils import Utils


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = config('UPLOAD_FOLDER')
HTTP_HOST = config('HTTP_HOST')
HTTP_PORT = config('HTTP_PORT')


utils = Utils()
yolo = None

class WebServer:

    def __init__(self, tf_yolo):
        global yolo
        yolo = tf_yolo


    def crossdomain(origin=None, methods=None, headers=None, max_age=21600,
                attach_to_all=True, automatic_options=True):
        """Decorator function that allows crossdomain requests.
            Courtesy of
            https://blog.skyred.fi/articles/better-crossdomain-snippet-for-flask.html
        """
        if methods is not None:
            methods = ', '.join(sorted(x.upper() for x in methods))
        # use str instead of basestring if using Python 3.x
        if headers is not None and not isinstance(headers, basestring):
            headers = ', '.join(x.upper() for x in headers)
        # use str instead of basestring if using Python 3.x
        if not isinstance(origin, str):
            origin = ', '.join(origin)
        if isinstance(max_age, timedelta):
            max_age = max_age.total_seconds()

        def get_methods():
            """ Determines which methods are allowed
            """
            if methods is not None:
                return methods

            options_resp = current_app.make_default_options_response()
            return options_resp.headers['allow']

        def decorator(f):
            """The decorator function
            """
            def wrapped_function(*args, **kwargs):
                """Caries out the actual cross domain code
                """
                if automatic_options and request.method == 'OPTIONS':
                    resp = current_app.make_default_options_response()
                else:
                    resp = make_response(f(*args, **kwargs))
                if not attach_to_all and request.method != 'OPTIONS':
                    return resp

                h = resp.headers
                h['Access-Control-Allow-Origin'] = origin
                h['Access-Control-Allow-Methods'] = get_methods()
                h['Access-Control-Max-Age'] = str(max_age)
                h['Access-Control-Allow-Credentials'] = 'true'
                h['Access-Control-Allow-Headers'] = \
                    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
                if headers is not None:
                    h['Access-Control-Allow-Headers'] = headers
                return resp

            f.provide_automatic_options = False
            return update_wrapper(wrapped_function, f)
        return decorator
    

    def startServer(self):
        global HTTP_HOST
        global HTTP_PORT
        print(" [*] Starting WebServer.")
        app.run(host=HTTP_HOST, debug=False, port=HTTP_PORT)

   
    @app.route('/ping', methods=['GET','OPTIONS'])
    @crossdomain(origin='*')
    def ping():
        message = {
            'status':True,
            'message':'pong'
        }
        return jsonify(message)

    @app.route('/getprediction', methods=['POST','OPTIONS'])
    @crossdomain(origin='*')
    def get_prediction():
        global yolo

        try:
            file = request.files['image']

            if file:
                img_name = secure_filename(file.filename)
                filename = os.path.join(app.config['UPLOAD_FOLDER'], img_name)
                file.save(filename)            
            else:
                 raise Exception("Não foi possível acessar os parametros da requisição")
           
            np_image = cv2.imread(filename)
            prediction = yolo.return_predict(np_image)

            os.remove(filename)

            for itens in prediction:
                itens['confidence'] = str(itens['confidence'])
            print(prediction)
            retorno = {
                'error': False,
                'detected': prediction
            }

            return jsonify(retorno)

        except Exception as e:
            retorno = {
                'error': True,
                'message': str(e)
            }
            
            return jsonify(retorno)

        
    @app.route('/getPredictionB64', methods=['POST','OPTIONS'])
    @crossdomain(origin='*')
    def get_prediction_base64():
        global yolo

        try:                        
            str_b64 =  request.json.get('image64')
            if(str_b64 == None):
                raise Exception("Foto não enviada. Parâmetro: photo")            
            
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], 'teste.png')
            b64_img_bytes = str_b64.encode('utf-8')

            with open(file_path, "wb") as fh:
                fh.write(base64.decodebytes(b64_img_bytes))


            np_image = cv2.imread(file_path)
            prediction = yolo.return_predict(np_image)

            
            for itens in prediction:
                itens['confidence'] = str(itens['confidence'])

            retorno = {
                'status': True,
                'error': False,
                'detected': prediction
            }

            os.remove(file_path)

            return jsonify(retorno)           

        except Exception as e:
            retorno = {
                'status' : False,
                'error': True,
                'message': str(e)
            }
            
            print(str(traceback.print_exc()))
            return jsonify(retorno)


    def main(self):    
        t1 = threading.Thread(target=self.startServer, daemon=True )
        t1.start()