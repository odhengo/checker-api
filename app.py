from decouple import config

from darkflow.net.build import TFNet
from webserver import WebServer
from odhen.utils import Utils


options = {
    "model": config('DARKFLOW_MODEL'), 
    "load": int(config('DARKFLOW_LOAD')), 
    "labels": config('DARKFLOW_LABELS'),
    "threshold": float(config('DARKFLOW_THRESHOLD')),
    "gpu": 0.45
}
tf_yolo = TFNet(options)

utils = Utils()
Utils.clearConsole()
Utils.printStart()


web_server = WebServer(tf_yolo)
web_server.main() 

while(True):
    pass