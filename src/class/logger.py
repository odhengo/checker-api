import os
import datetime
import logging
from decouple import config
import sys

LOG_PATH = config('LOG_PATH')
if not os.path.exists(LOG_PATH):
    os.makedirs(LOG_PATH)
LOG_FORMAT = config('LOG_FORMAT')
LOG_ATIVO = config('LOG_ATIVO')

class Logger:
    def log_enabled(func):
        def wrapper(*args, **kwargs):
            if LOG_ATIVO != "False":
                return func(*args, **kwargs)
        return wrapper

    def __init__(self,level=10):
        logging.basicConfig(
            level = level,
            format = LOG_FORMAT,
            handlers=[
                logging.FileHandler(LOG_PATH+"/log.log"),
                logging.StreamHandler()
            ]
            )
        self.logger = logging.getLogger()
    
    @log_enabled
    def info(self,message):
            self.logger.info(message)
    @log_enabled
    def debug(self,message):
        self.logger.debug(message)
    @log_enabled
    def error(self,message):
        self.logger.error(message)
