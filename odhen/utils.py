from os import system, name

import cv2

import requests 
import base64
import re
from termcolor import colored
from decouple import config


RABBIT_MQ_HOST = config('RABBIT_MQ_HOST')
RABBIT_MQ_PORT = config('RABBIT_MQ_PORT')


class Utils:
    

    def printStart():
        programName = """
               _____ _____ _  ___   _ ___ ____    _      ___    _    
              |_   _| ____| |/ | \ | |_ _/ ___|  / \    |_ _|  / \   
                | | |  _| | ' /|  \| || |\___ \ / _ \    | |  / _ \  
                | | | |___| . \| |\  || | ___) / ___ \   | | / ___ \ 
                |_| |_____|_|\_|_| \_|___|____/_/   \_\ |___/_/   \_\ 
        """

        print(colored(programName, 'green'))

    def clearConsole():  
        # for windows 
        if name == 'nt': 
            _ = system('cls') 

        # for mac and linux(here, os.name is 'posix') 
        else: 
            _ = system('clear')
            
    def get_prediction_image(self,image,predict):

        thickness=2

        for i in predict:
            left = int(i['topleft']['x'])
            top = int(i['topleft']['y'])
            right = int(i['bottomright']['x'])
            bot = int(i['bottomright']['y'])
            label = i['label']
            color =  (30,30,181)
            cv2.rectangle(image,(left, top), (right, bot),color,thickness)
            cv2.putText(image, label, (left, top - 12),0, 1e-3 * 600, color,7//3)

        ret, jpeg = cv2.imencode('.jpg', image)
        
        encoded_string = base64.b64encode(jpeg)
        return str(encoded_string)

    def get_product_cod(self,prod_label):
        try:
            base_url_interface = config('BASE_API_ODH_INTERFACE')
            endpoint = base_url_interface + 'menu/getproductcod'
            params = {
                "dslabelprod": prod_label
            }

            r = requests.post(endpoint, json = params)
            data = r.json()
            return data['cdproduto']
        except Exception as e:
            print("Erro ao buscar codigo do produto")
            print(str(e))
            return None

